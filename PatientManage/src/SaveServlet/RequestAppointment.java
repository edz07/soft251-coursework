package SaveServlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import data.DaoFactory;
import data.DataOperator;
import data.ManageData;
import pojo.Appointment;

/**
 *
 * @author Edwin
 */
@WebServlet(name = "RequestAppointmentServlet",urlPatterns ="/RequestAppointmentServlet")
public class RequestAppointment extends HttpServlet {
	private static final long serialVersionUID = 1L;   
	private DataOperator dataOperator = null;

    /**
     *
     * @param config
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException {
		dataOperator = DaoFactory.getSingleDataOperator();
	}
	
    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//HttpSession session = request.getSession();//创建Session对象
		//String rno = (String) session.getAttribute("RNO");//从Session对象中获得用户学号/工号
		//String name = (String) session.getAttribute("NAME");
		String patientID = request.getParameter("patientID");
		String doctorID = request.getParameter("doctorID");
		String startTime = request.getParameter("startTime");	
		String endTime = request.getParameter("endTime");
		
		ManageData.appointmentsList.add(new Appointment(patientID,doctorID,startTime, endTime));
		
		PrintWriter out = response.getWriter();
		out.println("<script language='javascript'>");
		out.println("var str='Request Appointment Success ！';");
		out.println("alert(str);");
		out.println("window.location.href='PatientPage.jsp';");
		out.println("</script>");	
		out.flush();
		out.close();
	}
}
