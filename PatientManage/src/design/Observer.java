package design;

import pojo.Appointment;

/**
 *
 * @author Edwin
 */
public interface Observer  {

    /**
     *
     * @param appointment
     */
    public void update(Appointment appointment);
}
