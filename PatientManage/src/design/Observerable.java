package design;

/**
 *
 * @author Edwin
 */
public interface Observerable {

    /**
     *
     * @param observer
     */
    public void registerObserber(Observer observer);

    /**
     *
     * @param observer
     */
    public void removeObserver(Observer observer);

    /**
     *
     */
    public void notifyObserver();
}
