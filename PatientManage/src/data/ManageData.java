package data;

import java.util.LinkedList;
import java.util.List;


import pojo.Administrator;
import pojo.Appointment;
import pojo.Doctor;
import pojo.Patient;
import pojo.Prescription;
import pojo.Secretary;

/**
 *
 * @author Edwin
 */
public class ManageData {

    /**
     *
     */
    public static List<Patient> patientList = new LinkedList<>();

    /**
     *
     */
    public static List<Doctor> doctorList = new LinkedList<>();

    /**
     *
     */
    public static List<Secretary> secretaryList = new LinkedList<>();

    /**
     *
     */
    public static List<Administrator> administratorList = new LinkedList<>();

    /**
     *
     */
    public static List<Appointment> appointmentsList = new LinkedList<>();

    /**
     *
     */
    public static List<Prescription> prescriptionsList = new LinkedList<>();

    /**
     *
     */
    public void init() {
		
		Patient john = new Patient("P1001","John");
		john.setAddress("Portland Square Building, Drake Circus, Plymouth, PL4 8AA");
		john.setGender("Male");
		john.setAge(45);
		john.setHistory("no other sicks!!!!");
		
		Patient frank = new Patient("P1002", "Frannk");
		Patient wade = new Patient("P1003", "Wade");
		
		Doctor james = new Doctor("D1001", "James");
		james.setAddress("Fictitious Clinic, Diagon Alley, Drake Circus, Plymouth, PL4 8AA");
		Appointment appointment = new Appointment("P1001","D1001","2019-1-1","2019-1-2");
		
		Doctor kobe = new Doctor("D1002","Kobe");
		Doctor curry = new Doctor("D1003","Curry");
		
		String medicine = "Amoxicillin";
		int quantity = 24;
		String dosage = "4 per day – at least 6 hours between each dose";
		Prescription prescription = new Prescription(medicine, quantity, dosage);
		prescription.setDoctorID("D1001");
		prescription.setPatientID("P1001");
		john.setPrescription(prescription);
		john.setAppointment(appointment);
		james.setAppointment(appointment);
		
		Administrator admin = new Administrator("A1111", "admin");
		Secretary secretary = new Secretary("S1001", "secret");
		
		ManageData.patientList.add(john);
		ManageData.patientList.add(frank);
		ManageData.patientList.add(wade);
		ManageData.doctorList.add(james);
		ManageData.doctorList.add(kobe);
		ManageData.doctorList.add(curry);
		ManageData.appointmentsList.add(appointment);
		ManageData.prescriptionsList.add(prescription);
		ManageData.administratorList.add(admin);
		ManageData.secretaryList.add(secretary);
	}
}