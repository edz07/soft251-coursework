<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Request Appointment</title>
</head>
<body bgcolor="#FFFFCC">
	<form name="requestAppointment_from" action="../RequestAppointmentServlet"
		method="post">
		<table align="center">
			<tr>
				<td align="center" colspan="2">
					<h3>Request Appointment</h3>
					<hr>
				</td>
			</tr>
			<tr>
				<td width="220px">Patient ID：</td>
				<td><input type="text" name="patientID" /></td>
			</tr>
			<tr>
				<td width="220px">Appointment's Doctor ID：</td>
				<td><input type="text" name="doctorID" /></td>
			</tr>
			<tr>
				<td width="220px">start appointment time：</td>
				<td><input type="text" name="startTime" /></td>
			</tr>
			<tr>
				<td width="220px">end appointment time：</td>
				<td><input type="text" name="endTime" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Request "></td>
			</tr>
		</table>
	</form>
</body>
</html>